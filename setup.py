import os
from setuptools import find_packages, setup

with open(os.path.join(os.path.dirname(__file__), 'README.md')) as readme:
    README = readme.read()

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name='python_google_calendar_api',
    version='0.3',
    packages=find_packages(),
    include_package_data=True,
    license='BSD License',
    description='A simple app to integrate Google calendar.',
    long_description=README,
    author='Yash Nagar',
    url='https://bitbucket.org/kingmray/django-google-calendar',
    install_requires=[
          'pytz','google-api-python-client','pyOpenSSL',
      ],
    classifiers = [],
)
